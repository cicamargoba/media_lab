/*  
 *  JTAG-WIFI with TCP-SERIAL by Emard <vordah@gmail.com>
 *
 *  OpenOCD compatible remote_bitbang WIFI-JTAG server
 *  for ESP8266 Arduino
 *
 *  OpenOCD interface:
 *
 *  interface remote_bitbang
 *  remote_bitbang_host jtag.lan
 *  remote_bitbang_port 3335
 *
 *  telnet jtag.lan 3335
 *  <ctrl-]>
 *  telnet> mode c
 *  <ctrl-@>
 *  mi32l>
 *
 *  virtual serial port:
 *  socat -d -d pty,link=/dev/ttyS5,raw,echo=0  tcp:jtag.lan:3335
 *
 *  send file to tcp
 *  socat -u FILE:blink.cpp.hex TCP:jtag.lan:3335
*/

/* 
  based on
  WiFiTelnetToSerial - Example Transparent UART to Telnet Server for esp8266
  by Hristo Gochkov
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.
  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <ESP8266WiFi.h>
#include <WebSocketServer.h>

const char* ssid = "ESP0";
const char* password = "12345678";

String id = ssid;
//#include "pass.h"
// password = "something"; // 8 characters or longer will use WPA2-PSK
// password = "";          // zero length password connects to open network

// cross reference gpio to nodemcu lua pin numbering
// esp       GPIO: 0, 2, 4, 5,15,13,12,14,16
// lua nodemcu  D: 3, 4, 2, 1, 8, 7, 6, 5, 0

// GPIO pin assignment
// Try to avoid connecting JTAG to GPIO 0, 2, 15, 16 (board may not boot)
// only 2 serial pin sets are possible:
// normal (if programming cable can be disconnected):  TXD=1   RXD=3  
// alternate (programming cable always connected):     TXD=15  RXD=13

// pinout suitable for bare ESP-7 or ESP-12 (JTAG pins on one side, direct to xilinx 14-pin)
enum { TDO=12, TDI=14, TCK=16, TMS=13, TRST=4, SRST=5, TXD=1, RXD=3, LED=2 };
// possible use of SPI for acceleration, fixed pins: TDO=12, TDI=13, TCK=14

// pinout for nodemcu-devkit, usbserial alwyas connected, LED
// boot problem: at power up, user has to manually disconnect TXD GPIO15, TRST GPIO0, SRST GPIO2
//     TDO=D6, TDI=D5,TCK=D2,TMS=D1,TRST=D3,SRST=D4  TXD=D8, RXD=D7
// enum { TDO=12, TDI=5, TCK=14, TMS=16, TRST=0, SRST=2, TXD=15, RXD=13, LED=1 };

enum { MODE_JTAG=0, MODE_SERIAL=1 };
uint8_t mode = MODE_JTAG; // initial input parser mode is JTAG (remote bitbang)

enum { CLIENT=0, SERVER=1 }; // SERVER: jtag is Access Point
uint8_t wifi_mode = SERVER;

// *** serial port settings ***
#define BAUDRATE 115200

// 0: don't use additional TX
// 1: use additional TX at GPIO2
// this option currently doesn't work, leaving it 0
#define TXD_GPIO2 0

//how many clients should be able to telnet to this ESP8266
#define MAX_SRV_CLIENTS 1

WiFiServer server(3335);
WiFiClient serverClients[MAX_SRV_CLIENTS];

// Define how many callback functions you have. Default is 1.
#define CALLBACK_FUNCTIONS 1

WebSocketServer webSocketServer;

// led logic
#define LED_ON LOW
#define LED_OFF HIGH
#define LED_DIM INPUT_PULLUP

uint8_t jtag_state = 0; // 0:jtag_off 1:jtag_on, initial is jtag_off

// activate JTAG outputs
void jtag_on(void) {
  // Serial.println("jtag on");
  pinMode(TDO, INPUT);
  pinMode(TDI, OUTPUT);
  pinMode(TMS, OUTPUT);
  pinMode(TRST, INPUT);
  pinMode(SRST, INPUT);
  pinMode(LED, OUTPUT);
  pinMode(TCK, OUTPUT);
  jtag_state = 1;
}

// deactivate JTAG outputs: all pins input
void jtag_off(void) {
  #ifdef DEBUG_RBB
    Serial.println("jtag off");
  #endif
  pinMode(TCK, INPUT);
  pinMode(TDO, INPUT);
  pinMode(TDI, INPUT);
  pinMode(TMS, INPUT);
  pinMode(TRST, INPUT);
  pinMode(SRST, INPUT);
  digitalWrite(LED, LED_OFF);
  pinMode(LED, INPUT);
  jtag_state = 0;
}

uint8_t jtag_read(void) {
  return digitalRead(TDO);
}

void jtag_write(uint8_t tck_tms_tdi) {
  digitalWrite(TDI, tck_tms_tdi & 1 ? HIGH : LOW);
  digitalWrite(TMS, tck_tms_tdi & 2 ? HIGH : LOW);
  digitalWrite(TCK, tck_tms_tdi & 4 ? HIGH : LOW);
}

void serial_break() {
  pinMode(LED, LED_DIM);
  digitalWrite(LED, LED_ON);
  if(TXD == 15)
    Serial.swap();
  Serial.end(); // shutdown serial port
  #if TXD_GPIO2
  // if we want to drive additional tx line
  Serial.end(); // shutdown it too
  #endif
  pinMode(TXD, OUTPUT);
  digitalWrite(TXD, LOW); // TXD line LOW for 200ms is serial break
  #if TXD_GPIO2
  pinMode(2, OUTPUT);
  digitalWrite(2, LOW);
  #endif
  delay(210); // at least 200ms we must wait for BREAK to take effect
  Serial.begin(BAUDRATE); // start port just before use
  // remove serial break either above or after swap
  if(TXD == 15)
    Serial.swap();
  #if TXD_GPIO2
  //Serial.begin(BAUDRATE); // port started, break removed at GPIO2 (will now become serial TX)
  #endif
  pinMode(LED, INPUT);
  digitalWrite(LED, LED_OFF);
  Serial.flush();
}

void tcp_parser(WiFiClient *client) {
  if(mode == MODE_JTAG) {
    char c = client->read();
    switch(c) {
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
        jtag_write(c & 7); // it's the same as ((c-'0') & 7)
        break;
      case 'R':
        client->write('0'+jtag_read());
        break;
      case 'r':
      case 's':
      case 't':
      case 'B':
        if(jtag_state == 0)
          jtag_on();
        digitalWrite(LED, LED_ON);
        break;
      case 'b':
        digitalWrite(LED, LED_OFF);
        break;
      case '\0': // ctrl-@ (byte 0x00) sends serial BREAK
        serial_break();
        break;
      case '\r': // press enter to tcp-serial mode
        jtag_off();
        mode = MODE_SERIAL;
        break;
      case 'Q':
        jtag_off();
        client->stop(); // disconnect client's TCP
        break;
    } /* end switch */
  } // end mode == JTAG
}

void serialPrint(String webSocketMessage) {
// get data from the telnet client and push it to the UART
  if(mode == MODE_SERIAL && webSocketMessage != "") {
    pinMode(LED, OUTPUT);
    digitalWrite(LED, LED_ON);
    Serial.print(webSocketMessage);
    digitalWrite(LED, LED_OFF);
  }
}

void setup() {
  jtag_off();
  pinMode(LED, OUTPUT);

    //start UART.
  Serial.begin(BAUDRATE);
  if(TXD == 15)
    Serial.swap();

  if(wifi_mode == CLIENT) {
    if(password[0] != '\0')
      WiFi.begin(ssid, password);
    else
      WiFi.begin(ssid);
    //Serial.print("\nConnecting to "); 
    //Serial.println(ssid);
    while (WiFi.status() != WL_CONNECTED) { 
      // blink LED when trying to connect
      digitalWrite(LED, LED_ON);
      delay(10);
      digitalWrite(LED, LED_OFF);
      delay(500);
    }
  }
  
  if(wifi_mode == SERVER) {
    if(password[0] != '\0')
      WiFi.softAP(ssid, password);
    else
      WiFi.softAP(ssid);
    //Serial.print("\nServing Access Point name "); Serial.println(ssid);
  }
  
  //start the server.
  server.begin();
  server.setNoDelay(true);
  
  //Serial.println("\n\nV2 Socket Response Test");  
  //Serial.println("\nReady! To connect, use:");
  //Serial.print("telnet ");
  //Serial.print("192.168.4.1:3335");
}

void loop() {
  uint8_t i = 0;
  String data = "";
  String uartMsg = "";
  //check if there are any new clients
  if (server.hasClient()) {
    for(i = 0; i < MAX_SRV_CLIENTS; i++) {
      //find free/disconnected spot
      if (!serverClients[i] || !serverClients[i].connected()) {
        if(serverClients[i]) {
          serverClients[i].stop();
          //Serial.print("\nClient "); Serial.print(i); Serial.print(" disconnected.\n\n"); 
        }
        serverClients[i] = server.available();
        //Serial.print("\nNew client: "); Serial.print(i); Serial.print(".\n\n"); 
        
        data = "";
        uartMsg = "";
        
        if (serverClients[i].connected()) {
          if(webSocketServer.handshake(serverClients[i])) {
            mode = MODE_SERIAL;
            //Serial.print("\n\nWebBrowser Client Connected - MODE_SERIAL.\n");
            if(jtag_state == 1)
              jtag_off();
              
            while (serverClients[i].connected()) {
              data = webSocketServer.getData();
              // Check for WebSocketServer receive data.
              if (data.length() > 0) {
                if(data == "rID") {
                  //Serial.print("\nID Request.");
                  webSocketServer.sendData("ID:" + id);
                } else if(data == "rCn") {
                  //Serial.print("\nConnection Request.");
                  webSocketServer.sendData("Cn:OK");
                } else if(data == "rUp") {
                  webSocketServer.sendData("Up:OK");
                  //wait all lines to return micropython echo.
                } else if(data == "rDis") {
                  break;
                } else {
                  serialPrint(data);
                  Serial.flush();
                  webSocketServer.sendData("cUp:OK");
                }
              }
              
              // Check UART for data.
              if(Serial.available() > 0 && mode == MODE_SERIAL) {
                uartMsg = Serial.readStringUntil('\n');
                webSocketServer.sendData(uartMsg);
              }
            }
            continue;
          } else {
            //Serial.print("\n\nNo WebBrowser Client Connected - MODE_JTAG.\n");
            mode = MODE_JTAG;
            if(jtag_state == 0)
              jtag_on();
            continue;
          }
        }
      }
    }
    
    //no free/disconnected spot so reject
    WiFiClient serverClient = server.available();
    serverClient.stop();
  }
  
  //check clients for data
  for(i = 0; i < MAX_SRV_CLIENTS; i++) {
    if (serverClients[i] && serverClients[i].connected()) {
      if(serverClients[i].available()) {
        //get data from the telnet client and push it to the UART
        while(serverClients[i].available()) 
          tcp_parser(&(serverClients[i]));
      }
    }
  }
}
