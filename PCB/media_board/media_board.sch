EESchema Schematic File Version 2
LIBS:stamp_mini_4_layers-cache
LIBS:con-jack
LIBS:adm3101e
LIBS:microsd
LIBS:transistor-npn
LIBS:ipc-7351-transistor
LIBS:switch-misc
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:micron_ddr_512Mb
LIBS:iMX23
LIBS:sdmmc
LIBS:usbconn
LIBS:fsusb20
LIBS:r_pack2
LIBS:pasives-connectors
LIBS:EEPROM
LIBS:PWR
LIBS:m25p32
LIBS:PROpendous-cache
LIBS:w_analog
LIBS:gl850g
LIBS:srf2012
LIBS:rclamp0502b
LIBS:mcp130
LIBS:ABM8G
LIBS:usb_a
LIBS:Reset
LIBS:stm32f100vxx
LIBS:lt1117cst
LIBS:zxmhc3f381n8
LIBS:fsusb43
LIBS:usb-mini
LIBS:atsam3n0aa-au
LIBS:drv8835
LIBS:ftdichip
LIBS:stm32f4_lqfp100
LIBS:cstcr6m00g53z
LIBS:ESP8266
LIBS:LM2623
LIBS:lm3478
LIBS:media_board-cache
EELAYER 25 0
EELAYER END
$Descr User 5512 5512
encoding utf-8
Sheet 1 4
Title ""
Date "9 may 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	2450 1600 3550 1600
Wire Wire Line
	2450 1400 3550 1400
Wire Wire Line
	2450 1500 3550 1500
Wire Wire Line
	2450 1700 3550 1700
Wire Wire Line
	2450 1800 3550 1800
Wire Wire Line
	2450 2050 3550 2050
Wire Wire Line
	2450 2150 3550 2150
$Sheet
S 3550 1100 1150 1250
U 5439B76D
F0 "serial_interface" 60
F1 "serial_interface.sch" 60
F2 "TCK" I L 3550 1700 60 
F3 "TDI" I L 3550 1600 60 
F4 "TDO" I L 3550 1500 60 
F5 "TMS" I L 3550 1800 60 
F6 "nTRST" I L 3550 1400 60 
F7 "nSRST" I L 3550 1900 60 
F8 "RX" I L 3550 2050 60 
F9 "TX" I L 3550 2150 60 
F10 "RST_ESP" I L 3550 1250 60 
$EndSheet
$Comp
L CONN_1 HS1
U 1 1 543C7615
P 3650 2650
F 0 "HS1" H 3730 2650 40  0000 L CNN
F 1 "CONN_1" H 3650 2705 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 3650 2650 60  0001 C CNN
F 3 "" H 3650 2650 60  0000 C CNN
	1    3650 2650
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 HS2
U 1 1 543C768A
P 3650 2750
F 0 "HS2" H 3730 2750 40  0000 L CNN
F 1 "CONN_1" H 3650 2805 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 3650 2750 60  0001 C CNN
F 3 "" H 3650 2750 60  0000 C CNN
	1    3650 2750
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 HS3
U 1 1 543C76A6
P 3650 2850
F 0 "HS3" H 3730 2850 40  0000 L CNN
F 1 "CONN_1" H 3650 2905 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 3650 2850 60  0001 C CNN
F 3 "" H 3650 2850 60  0000 C CNN
	1    3650 2850
	1    0    0    -1  
$EndComp
$Comp
L CONN_1 HS4
U 1 1 543C76BF
P 3650 2950
F 0 "HS4" H 3730 2950 40  0000 L CNN
F 1 "CONN_1" H 3650 3005 30  0001 C CNN
F 2 "opendous:1Pin_TH_40milVia" H 3650 2950 60  0001 C CNN
F 3 "" H 3650 2950 60  0000 C CNN
	1    3650 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2650 3400 2650
Wire Wire Line
	3400 2650 3400 3000
Wire Wire Line
	3400 2750 3500 2750
Wire Wire Line
	3400 2850 3500 2850
Connection ~ 3400 2750
Wire Wire Line
	3400 2950 3500 2950
Connection ~ 3400 2850
$Comp
L GND-RESCUE-stamp_mini_4_layers #PWR01
U 1 1 543C894D
P 3400 3000
F 0 "#PWR01" H 3400 3000 30  0001 C CNN
F 1 "GND" H 3400 2930 30  0001 C CNN
F 2 "" H 3400 3000 60  0001 C CNN
F 3 "" H 3400 3000 60  0001 C CNN
	1    3400 3000
	1    0    0    -1  
$EndComp
Connection ~ 3400 2950
$Sheet
S 1100 1100 1350 1250
U 4E78AF25
F0 "SAM3M" 60
F1 "SAM3N.sch" 60
F2 "JNRST" B R 2450 1400 60 
F3 "JTMS" B R 2450 1800 60 
F4 "JTCK" B R 2450 1700 60 
F5 "JTDO" B R 2450 1500 60 
F6 "JTDI" B R 2450 1600 60 
F7 "STM_UART_TX" B R 2450 2050 60 
F8 "STM_UART_RX" B R 2450 2150 60 
F9 "NRESET" B R 2450 1900 60 
F10 "VM" I R 2450 2250 60 
F11 "RST_ESP" I R 2450 1250 60 
$EndSheet
Wire Wire Line
	2450 1900 3550 1900
Wire Wire Line
	2450 2250 2950 2250
$Comp
L R-RESCUE-stamp_mini_4_layers RRST1
U 1 1 55F65ACF
P 2950 1250
F 0 "RRST1" V 3030 1250 50  0000 C CNN
F 1 "1k" V 2950 1250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 2880 1250 30  0001 C CNN
F 3 "" H 2950 1250 30  0000 C CNN
	1    2950 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	2450 1250 2700 1250
Wire Wire Line
	3200 1250 3550 1250
$Sheet
S 1100 2800 1350 350 
U 56189EDE
F0 "power" 60
F1 "power.sch" 60
F2 "VM" I R 2450 2950 60 
$EndSheet
Wire Wire Line
	2450 2950 2950 2950
Wire Wire Line
	2950 2950 2950 2250
$EndSCHEMATC
