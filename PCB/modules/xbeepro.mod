PCBNEW-LibModule-V1  lun 17 jun 2013 10:23:27 COT
# encoding utf-8
Units mm
$INDEX
xbeepro
$EndINDEX
$MODULE xbeepro
Po 0 0 0 15 51BF29E2 00000000 ~~
Li xbeepro
Sc 0
AR 
Op 0 0 0
T0 0.22 4.98 1.524 1.524 0 0.3048 N V 21 N "xbeepro"
T1 0 0 1.524 1.524 0 0.3048 N V 21 N "VAL**"
DS -2.36 -6.53 3.09 -6.54 0.381 21
DS -10.94 19.3 -1.25 19.3 0.381 21
DS -1.25 19.3 11.29 19.3 0.381 21
DS 11.29 19.3 12.07 19.3 0.381 21
DS 12.07 19.3 12.28 19.3 0.381 21
DS 12.28 19.3 12.28 17.03 0.381 21
DS 12.28 17.03 12.24 9.68 0.381 21
DS 12.24 9.68 12.26 -0.53 0.381 21
DS 12.26 -0.53 12.26 -1.15 0.381 21
DS 12.26 -1.15 3.06 -6.55 0.381 21
DS -10.96 19.3 -12.08 19.3 0.381 21
DS -12.08 19.3 -12.25 19.3 0.381 21
DS -12.25 19.3 -12.25 14.1 0.381 21
DS -12.25 14.1 -12.26 -0.41 0.381 21
DS -12.26 -0.41 -12.26 -1.07 0.381 21
DS -12.26 -1.07 -3.01 -6.53 0.381 21
DS -3.01 -6.53 -2.36 -6.53 0.381 21
$PAD
Sh "10" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 18
$EndPAD
$PAD
Sh "11" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 18
$EndPAD
$PAD
Sh "20" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 0
$EndPAD
$PAD
Sh "19" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 2
$EndPAD
$PAD
Sh "17" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 6
$EndPAD
$PAD
Sh "18" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 4
$EndPAD
$PAD
Sh "14" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 12
$EndPAD
$PAD
Sh "15" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 10
$EndPAD
$PAD
Sh "16" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 8
$EndPAD
$PAD
Sh "12" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 16
$EndPAD
$PAD
Sh "13" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11 14
$EndPAD
$PAD
Sh "8" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 14
$EndPAD
$PAD
Sh "9" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 16
$EndPAD
$PAD
Sh "5" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 8
$EndPAD
$PAD
Sh "7" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 12
$EndPAD
$PAD
Sh "6" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 10
$EndPAD
$PAD
Sh "3" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 4
$EndPAD
$PAD
Sh "4" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 6
$EndPAD
$PAD
Sh "2" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 2
$EndPAD
$PAD
Sh "1" C 1.5 1.5 0 0 0
Dr 0.65 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11 0
$EndPAD
$EndMODULE xbeepro
$EndLIBRARY
