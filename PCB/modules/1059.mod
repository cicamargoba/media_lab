PCBNEW-LibModule-V1  jue 11 sep 2014 17:33:01 COT
# encoding utf-8
Units mm
$INDEX
1059
$EndINDEX
$MODULE 1059
Po 0 0 0 15 5412230B 00000000 ~~
Li 1059
Sc 0
AR /4E78AF25/54120DCB
Op 0 0 0
T0 10.99 -2.51 1.524 1.524 0 0.3048 N V 21 N "BAT1"
T1 13.58 0.76 1.524 1.524 0 0.3048 N V 21 N "BATTERY"
DS 27.94 0 27.94 8.128 0.381 21
DS 27.94 8.128 -1.016 8.128 0.381 21
DS -1.016 0 -1.016 8.382 0.381 21
DS 27.94 0 27.94 -8.128 0.381 21
DS 27.94 -8.128 -1.016 -8.128 0.381 21
DS -1.016 0 -1.016 -8.128 0.381 21
$PAD
Sh "1" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 1 "GND"
Po 26.92 -1.27
$EndPAD
$PAD
Sh "2" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 2 "N-00000134"
Po 0 0
$EndPAD
$PAD
Sh "1" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 1 "GND"
Po 26.92 1.27
$EndPAD
$EndMODULE 1059
$EndLIBRARY
