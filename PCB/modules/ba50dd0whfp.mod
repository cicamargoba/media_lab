PCBNEW-LibModule-V1  lun 17 jun 2013 09:06:59 COT
# encoding utf-8
Units mm
$INDEX
ba50dd0whfp
$EndINDEX
$MODULE ba50dd0whfp
Po 0 0 0 15 51BF1793 00000000 ~~
Li ba50dd0whfp
Sc 0
AR 
Op 0 0 0
T0 13.27 2.77 1.524 1.524 0 0.3048 N V 21 N "ba50dd0whfp"
T1 12.81 1.09 1.524 1.524 0 0.3048 N V 21 N "VAL**"
DS -5.26 -10.88 5.25 -10.87 0.381 21
DS 5.25 -10.87 5.26 -1.37 0.381 21
DS 5.26 -1.37 4.44 -1.37 0.381 21
DS 4.44 -1.37 4.44 0.82 0.381 21
DS 4.44 0.82 4.44 1.02 0.381 21
DS 4.44 1.02 -4.36 1.02 0.381 21
DS -4.36 1.02 -4.36 -1.35 0.381 21
DS -4.36 -1.35 -5.32 -1.35 0.381 21
DS -5.32 -1.35 -5.32 -10.9 0.381 21
$PAD
Sh "5" R 1.07 1.37 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.44 0
$EndPAD
$PAD
Sh "4" R 1.07 1.37 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.72 0
$EndPAD
$PAD
Sh "3" R 1.07 1.37 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" R 1.07 1.37 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.72 0
$EndPAD
$PAD
Sh "1" R 1.07 1.37 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.44 0
$EndPAD
$PAD
Sh "FIN" R 10 9 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -6.125
$EndPAD
$EndMODULE ba50dd0whfp
$EndLIBRARY
