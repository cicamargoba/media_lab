EESchema Schematic File Version 2
LIBS:chip_media-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chip_pro
LIBS:stm32f100vxx
LIBS:ABM8G
LIBS:drv8835
LIBS:lt1117cst
LIBS:stm32f0
LIBS:Carlolib-dev
LIBS:opendous
LIBS:mem_mic
LIBS:usba-plug
LIBS:imx233stamp-cache
LIBS:srf2012
LIBS:rclamp0502b
LIBS:usbconn
LIBS:usb_a
LIBS:onion2_new
LIBS:onion2
LIBS:JQC_3FF
LIBS:tcs3404
LIBS:sp481
LIBS:vof_6b_s5
LIBS:chip_media-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L TL072 UPH1
U 1 1 59A02D48
P 1650 1525
F 0 "UPH1" H 1725 1875 50  0000 L CNN
F 1 "TL072" H 1725 1775 50  0000 L CNN
F 2 "Housings_SOIC:SO-8_5.3x6.2mm_Pitch1.27mm" H 1650 1525 50  0001 C CNN
F 3 "" H 1650 1525 50  0000 C CNN
	1    1650 1525
	1    0    0    -1  
$EndComp
$Comp
L TL072 UPH1
U 2 1 59A02D92
P 3000 1425
F 0 "UPH1" H 3100 1725 50  0000 L CNN
F 1 "TL072" H 3125 1625 50  0000 L CNN
F 2 "" H 3000 1425 50  0000 C CNN
F 3 "" H 3000 1425 50  0000 C CNN
	2    3000 1425
	1    0    0    -1  
$EndComp
$Comp
L TPS60403 UPW1
U 1 1 59A02E20
P 5200 1425
F 0 "UPW1" H 5525 1750 40  0000 L CNN
F 1 "TPS60400" H 5525 1675 40  0000 L CNN
F 2 "opendous:SOT23-5_Opendous" H 5200 1425 60  0001 C CNN
F 3 "" H 5200 1425 60  0001 C CNN
	1    5200 1425
	1    0    0    -1  
$EndComp
$Comp
L TL074 UEC1
U 1 1 59A03148
P 2025 3900
F 0 "UEC1" H 2200 4400 50  0000 L CNN
F 1 "TL074" H 2225 4300 50  0000 L CNN
F 2 "Housings_SSOP:TSSOP-14_4.4x5mm_Pitch0.65mm" H 1975 4000 50  0001 C CNN
F 3 "" H 2075 4100 50  0000 C CNN
	1    2025 3900
	1    0    0    -1  
$EndComp
$Comp
L TL074 UEC1
U 2 1 59A031BE
P 3650 4000
F 0 "UEC1" H 3925 4350 50  0000 L CNN
F 1 "TL074" H 3925 4250 50  0000 L CNN
F 2 "SMD_Packages:SOIC-14_N" H 3600 4100 50  0001 C CNN
F 3 "" H 3700 4200 50  0000 C CNN
	2    3650 4000
	1    0    0    -1  
$EndComp
$Comp
L TL074 UEC1
U 3 1 59A0327D
P 5150 3575
F 0 "UEC1" H 5491 3621 50  0000 L CNN
F 1 "TL074" H 5491 3530 50  0000 L CNN
F 2 "" H 5100 3675 50  0000 C CNN
F 3 "" H 5200 3775 50  0000 C CNN
	3    5150 3575
	1    0    0    -1  
$EndComp
$Comp
L TL074 UEC1
U 4 1 59A03311
P 5125 4525
F 0 "UEC1" H 5275 4900 50  0000 L CNN
F 1 "TL074" H 5275 4800 50  0000 L CNN
F 2 "" H 5075 4625 50  0000 C CNN
F 3 "" H 5175 4725 50  0000 C CNN
	4    5125 4525
	1    0    0    -1  
$EndComp
$Comp
L TPS60403 UPW2
U 1 1 59A03429
P 7925 4100
F 0 "UPW2" H 8300 4450 40  0000 L CNN
F 1 "TPS60400" H 8300 4375 40  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 7925 4100 60  0001 C CNN
F 3 "" H 7925 4100 60  0001 C CNN
	1    7925 4100
	1    0    0    -1  
$EndComp
$Comp
L C CPH1
U 1 1 59A05BFA
P 1450 2275
F 0 "CPH1" H 1500 2375 50  0000 L CNN
F 1 "100nF" H 1500 2175 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1450 2275 60  0001 C CNN
F 3 "" H 1450 2275 60  0001 C CNN
	1    1450 2275
	0    1    1    0   
$EndComp
$Comp
L R RPH1
U 1 1 59A05C0E
P 975 1625
F 0 "RPH1" V 1055 1625 50  0000 C CNN
F 1 "33k" V 975 1625 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 975 1625 60  0001 C CNN
F 3 "" H 975 1625 60  0001 C CNN
	1    975  1625
	0    -1   -1   0   
$EndComp
$Comp
L R RPH2
U 1 1 59A0610E
P 1425 2050
F 0 "RPH2" V 1505 2050 50  0000 C CNN
F 1 "200k" V 1425 2050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1425 2050 60  0001 C CNN
F 3 "" H 1425 2050 60  0001 C CNN
	1    1425 2050
	0    -1   -1   0   
$EndComp
$Comp
L R RPH3
U 1 1 59A06724
P 2225 1525
F 0 "RPH3" V 2305 1525 50  0000 C CNN
F 1 "200k" V 2225 1525 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 2225 1525 60  0001 C CNN
F 3 "" H 2225 1525 60  0001 C CNN
	1    2225 1525
	0    -1   -1   0   
$EndComp
$Comp
L R RPH6
U 1 1 59A06A56
P 2975 1975
F 0 "RPH6" V 3055 1975 50  0000 C CNN
F 1 "200k" V 2975 1975 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 2975 1975 60  0001 C CNN
F 3 "" H 2975 1975 60  0001 C CNN
	1    2975 1975
	0    -1   -1   0   
$EndComp
$Comp
L R RPH7
U 1 1 59A06B61
P 3650 1425
F 0 "RPH7" V 3730 1425 50  0000 C CNN
F 1 "4.7k" V 3650 1425 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3650 1425 60  0001 C CNN
F 3 "" H 3650 1425 60  0001 C CNN
	1    3650 1425
	0    -1   -1   0   
$EndComp
$Comp
L R RPH5
U 1 1 59A06D97
P 2825 975
F 0 "RPH5" V 2905 975 50  0000 C CNN
F 1 "1k" V 2825 975 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 2825 975 60  0001 C CNN
F 3 "" H 2825 975 60  0001 C CNN
	1    2825 975 
	0    -1   -1   0   
$EndComp
$Comp
L R RPH4
U 1 1 59A06EAF
P 2250 975
F 0 "RPH4" V 2330 975 50  0000 C CNN
F 1 "3k" V 2250 975 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 2250 975 60  0001 C CNN
F 3 "" H 2250 975 60  0001 C CNN
	1    2250 975 
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR069
U 1 1 59A07001
P 3050 975
F 0 "#PWR069" H 3050 975 30  0001 C CNN
F 1 "GND" H 3050 905 30  0001 C CNN
F 2 "" H 3050 975 60  0001 C CNN
F 3 "" H 3050 975 60  0001 C CNN
	1    3050 975 
	0    -1   -1   0   
$EndComp
$Comp
L +5V #PWR070
U 1 1 59A078E9
P 1975 975
F 0 "#PWR070" H 1975 825 50  0001 C CNN
F 1 "+5V" H 1990 1148 50  0000 C CNN
F 2 "" H 1975 975 50  0000 C CNN
F 3 "" H 1975 975 50  0000 C CNN
	1    1975 975 
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR071
U 1 1 59A07EEC
P 675 1775
F 0 "#PWR071" H 675 1775 30  0001 C CNN
F 1 "GND" H 675 1705 30  0001 C CNN
F 2 "" H 675 1775 60  0001 C CNN
F 3 "" H 675 1775 60  0001 C CNN
	1    675  1775
	1    0    0    -1  
$EndComp
$Comp
L C C102
U 1 1 59A08A9D
P 5175 750
F 0 "C102" H 5225 850 50  0000 L CNN
F 1 "1uF" H 5225 650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5175 750 60  0001 C CNN
F 3 "" H 5175 750 60  0001 C CNN
	1    5175 750 
	0    1    1    0   
$EndComp
$Comp
L GND #PWR072
U 1 1 59A08DF9
P 5200 1675
F 0 "#PWR072" H 5200 1675 30  0001 C CNN
F 1 "GND" H 5200 1605 30  0001 C CNN
F 2 "" H 5200 1675 60  0001 C CNN
F 3 "" H 5200 1675 60  0001 C CNN
	1    5200 1675
	1    0    0    -1  
$EndComp
$Comp
L C C100
U 1 1 59A09167
P 4700 1650
F 0 "C100" H 4750 1750 50  0000 L CNN
F 1 "1uF" H 4750 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4700 1650 60  0001 C CNN
F 3 "" H 4700 1650 60  0001 C CNN
	1    4700 1650
	1    0    0    -1  
$EndComp
$Comp
L C C101
U 1 1 59A095DF
P 5775 1650
F 0 "C101" H 5825 1750 50  0000 L CNN
F 1 "1uF" H 5825 1550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5775 1650 60  0001 C CNN
F 3 "" H 5775 1650 60  0001 C CNN
	1    5775 1650
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR073
U 1 1 59A09806
P 4700 1825
F 0 "#PWR073" H 4700 1825 30  0001 C CNN
F 1 "GND" H 4700 1755 30  0001 C CNN
F 2 "" H 4700 1825 60  0001 C CNN
F 3 "" H 4700 1825 60  0001 C CNN
	1    4700 1825
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR074
U 1 1 59A09822
P 5775 1850
F 0 "#PWR074" H 5775 1850 30  0001 C CNN
F 1 "GND" H 5775 1780 30  0001 C CNN
F 2 "" H 5775 1850 60  0001 C CNN
F 3 "" H 5775 1850 60  0001 C CNN
	1    5775 1850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR075
U 1 1 59A09E29
P 4475 1425
F 0 "#PWR075" H 4475 1275 50  0001 C CNN
F 1 "+5V" H 4490 1598 50  0000 C CNN
F 2 "" H 4475 1425 50  0000 C CNN
F 3 "" H 4475 1425 50  0000 C CNN
	1    4475 1425
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR076
U 1 1 59A0A29A
P 1550 1150
F 0 "#PWR076" H 1550 1000 50  0001 C CNN
F 1 "+5V" H 1565 1323 50  0000 C CNN
F 2 "" H 1550 1150 50  0000 C CNN
F 3 "" H 1550 1150 50  0000 C CNN
	1    1550 1150
	1    0    0    -1  
$EndComp
$Comp
L R RC1
U 1 1 59A10A98
P 725 3800
F 0 "RC1" V 805 3800 50  0000 C CNN
F 1 "1k" V 725 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 725 3800 60  0001 C CNN
F 3 "" H 725 3800 60  0001 C CNN
	1    725  3800
	-1   0    0    1   
$EndComp
$Comp
L C CC1
U 1 1 59A10BD7
P 1025 3800
F 0 "CC1" V 875 3725 50  0000 L CNN
F 1 "100nF" V 1175 3700 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1025 3800 60  0001 C CNN
F 3 "" H 1025 3800 60  0001 C CNN
	1    1025 3800
	-1   0    0    1   
$EndComp
$Comp
L R RC2
U 1 1 59A110D5
P 1350 4200
F 0 "RC2" V 1430 4200 50  0000 C CNN
F 1 "10k" V 1350 4200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1350 4200 60  0001 C CNN
F 3 "" H 1350 4200 60  0001 C CNN
	1    1350 4200
	0    -1   -1   0   
$EndComp
$Comp
L C CC2
U 1 1 59A11616
P 1850 3200
F 0 "CC2" V 1700 3125 50  0000 L CNN
F 1 "100nF" V 2000 3100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 1850 3200 60  0001 C CNN
F 3 "" H 1850 3200 60  0001 C CNN
	1    1850 3200
	0    -1   -1   0   
$EndComp
$Comp
L R RC3
U 1 1 59A116BE
P 2200 3200
F 0 "RC3" V 2280 3200 50  0000 C CNN
F 1 "1k" V 2200 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 2200 3200 60  0001 C CNN
F 3 "" H 2200 3200 60  0001 C CNN
	1    2200 3200
	0    -1   -1   0   
$EndComp
$Comp
L R RC5
U 1 1 59A11BAF
P 2750 3900
F 0 "RC5" V 2830 3900 50  0000 C CNN
F 1 "1k" V 2750 3900 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 2750 3900 60  0001 C CNN
F 3 "" H 2750 3900 60  0001 C CNN
	1    2750 3900
	0    -1   -1   0   
$EndComp
$Comp
L R RC6
U 1 1 59A11DE7
P 3025 3550
F 0 "RC6" V 3105 3550 50  0000 C CNN
F 1 "1k" V 3025 3550 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3025 3550 60  0001 C CNN
F 3 "" H 3025 3550 60  0001 C CNN
	1    3025 3550
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR077
U 1 1 59A12316
P 3025 3250
F 0 "#PWR077" H 3025 3250 30  0001 C CNN
F 1 "GND" H 3025 3180 30  0001 C CNN
F 2 "" H 3025 3250 60  0001 C CNN
F 3 "" H 3025 3250 60  0001 C CNN
	1    3025 3250
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR078
U 1 1 59A12655
P 725 4100
F 0 "#PWR078" H 725 4100 30  0001 C CNN
F 1 "GND" H 725 4030 30  0001 C CNN
F 2 "" H 725 4100 60  0001 C CNN
F 3 "" H 725 4100 60  0001 C CNN
	1    725  4100
	1    0    0    -1  
$EndComp
$Comp
L R RC7
U 1 1 59A12CB1
P 3775 4500
F 0 "RC7" V 3855 4500 50  0000 C CNN
F 1 "1k" V 3775 4500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 3775 4500 60  0001 C CNN
F 3 "" H 3775 4500 60  0001 C CNN
	1    3775 4500
	0    -1   -1   0   
$EndComp
$Comp
L R RC4
U 1 1 59A134B7
P 1900 4475
F 0 "RC4" V 1980 4475 50  0000 C CNN
F 1 "22k" V 1900 4475 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1900 4475 60  0001 C CNN
F 3 "" H 1900 4475 60  0001 C CNN
	1    1900 4475
	0    -1   -1   0   
$EndComp
$Comp
L C CC3
U 1 1 59A13990
P 2250 4750
F 0 "CC3" V 2100 4675 50  0000 L CNN
F 1 "100nF" V 2400 4650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 2250 4750 60  0001 C CNN
F 3 "" H 2250 4750 60  0001 C CNN
	1    2250 4750
	0    -1   -1   0   
$EndComp
$Comp
L D_ALT DC3
U 1 1 59A0EB57
P 5700 4525
F 0 "DC3" H 5700 4309 50  0000 C CNN
F 1 "MBR0540TP" H 5700 4400 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-123" H 5700 4401 50  0001 C CNN
F 3 "" H 5700 4525 50  0000 C CNN
	1    5700 4525
	-1   0    0    1   
$EndComp
$Comp
L R RC8
U 1 1 59A0F5BB
P 6200 4725
F 0 "RC8" V 6280 4725 50  0000 C CNN
F 1 "10k" V 6200 4725 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 6200 4725 60  0001 C CNN
F 3 "" H 6200 4725 60  0001 C CNN
	1    6200 4725
	-1   0    0    1   
$EndComp
$Comp
L C CC4
U 1 1 59A0F5C1
P 6500 4725
F 0 "CC4" V 6350 4650 50  0000 L CNN
F 1 "1uF" V 6650 4625 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 6500 4725 60  0001 C CNN
F 3 "" H 6500 4725 60  0001 C CNN
	1    6500 4725
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR079
U 1 1 59A0F5D0
P 6200 5025
F 0 "#PWR079" H 6200 5025 30  0001 C CNN
F 1 "GND" H 6200 4955 30  0001 C CNN
F 2 "" H 6200 5025 60  0001 C CNN
F 3 "" H 6200 5025 60  0001 C CNN
	1    6200 5025
	1    0    0    -1  
$EndComp
$Comp
L D_ALT DC1
U 1 1 59A1102F
P 1825 4750
F 0 "DC1" H 1875 4625 50  0000 C CNN
F 1 "IN41418" H 1850 4875 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-323F" H 1825 4750 50  0001 C CNN
F 3 "" H 1825 4750 50  0000 C CNN
	1    1825 4750
	-1   0    0    1   
$EndComp
Text Notes 1375 5425 0    60   ~ 0
1N4148WSFSCT-ND\n
$Comp
L D_ALT DC2
U 1 1 59A11646
P 1825 5075
F 0 "DC2" H 1875 4950 50  0000 C CNN
F 1 "IN41418" H 1850 5200 50  0000 C CNN
F 2 "Diodes_SMD:D_SOD-323F" H 1825 5075 50  0001 C CNN
F 3 "" H 1825 5075 50  0000 C CNN
	1    1825 5075
	1    0    0    -1  
$EndComp
Text Notes 5850 4200 0    60   ~ 0
MBR0540TPMSCT-ND\n
$Comp
L C CC7
U 1 1 59A12D4D
P 7900 3425
F 0 "CC7" H 7950 3525 50  0000 L CNN
F 1 "1uF" H 7950 3325 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7900 3425 60  0001 C CNN
F 3 "" H 7900 3425 60  0001 C CNN
	1    7900 3425
	0    1    1    0   
$EndComp
$Comp
L GND #PWR080
U 1 1 59A12D53
P 7925 4350
F 0 "#PWR080" H 7925 4350 30  0001 C CNN
F 1 "GND" H 7925 4280 30  0001 C CNN
F 2 "" H 7925 4350 60  0001 C CNN
F 3 "" H 7925 4350 60  0001 C CNN
	1    7925 4350
	1    0    0    -1  
$EndComp
$Comp
L C CC5
U 1 1 59A12D59
P 7425 4325
F 0 "CC5" H 7475 4425 50  0000 L CNN
F 1 "1uF" H 7475 4225 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 7425 4325 60  0001 C CNN
F 3 "" H 7425 4325 60  0001 C CNN
	1    7425 4325
	1    0    0    -1  
$EndComp
$Comp
L C CC6
U 1 1 59A12D5F
P 8500 4325
F 0 "CC6" H 8550 4425 50  0000 L CNN
F 1 "1uF" H 8550 4225 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 8500 4325 60  0001 C CNN
F 3 "" H 8500 4325 60  0001 C CNN
	1    8500 4325
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR081
U 1 1 59A12D65
P 7425 4500
F 0 "#PWR081" H 7425 4500 30  0001 C CNN
F 1 "GND" H 7425 4430 30  0001 C CNN
F 2 "" H 7425 4500 60  0001 C CNN
F 3 "" H 7425 4500 60  0001 C CNN
	1    7425 4500
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR082
U 1 1 59A12D6B
P 8500 4525
F 0 "#PWR082" H 8500 4525 30  0001 C CNN
F 1 "GND" H 8500 4455 30  0001 C CNN
F 2 "" H 8500 4525 60  0001 C CNN
F 3 "" H 8500 4525 60  0001 C CNN
	1    8500 4525
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR083
U 1 1 59A12D71
P 7200 4100
F 0 "#PWR083" H 7200 3950 50  0001 C CNN
F 1 "+5V" H 7215 4273 50  0000 C CNN
F 2 "" H 7200 4100 50  0000 C CNN
F 3 "" H 7200 4100 50  0000 C CNN
	1    7200 4100
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR084
U 1 1 59A14CBA
P 1925 3600
F 0 "#PWR084" H 1925 3450 50  0001 C CNN
F 1 "+5V" H 1940 3773 50  0000 C CNN
F 2 "" H 1925 3600 50  0000 C CNN
F 3 "" H 1925 3600 50  0000 C CNN
	1    1925 3600
	1    0    0    -1  
$EndComp
Text Label 1550 1900 0    60   ~ 0
-5PH
Text Label 5925 1425 0    60   ~ 0
-5PH
Text Label 1925 4250 0    60   ~ 0
-5EC
Text Label 8650 4100 0    60   ~ 0
-5EC
Text HLabel 3150 4500 0    60   Input ~ 0
EC_PROBE
Text HLabel 6675 4525 2    60   Output ~ 0
EC_VOUT
Text HLabel 1200 1425 0    60   Input ~ 0
PH_PROBE
Text HLabel 3900 1425 2    60   Output ~ 0
PH_VOUT
$Comp
L GND #PWR085
U 1 1 59A81671
P 1250 6375
F 0 "#PWR085" H 1250 6375 30  0001 C CNN
F 1 "GND" H 1250 6305 30  0001 C CNN
F 2 "" H 1250 6375 60  0001 C CNN
F 3 "" H 1250 6375 60  0001 C CNN
	1    1250 6375
	1    0    0    -1  
$EndComp
$Comp
L R RT1
U 1 1 59A81E0C
P 1250 5950
F 0 "RT1" V 1330 5950 50  0000 C CNN
F 1 "47k" V 1250 5950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" H 1250 5950 60  0001 C CNN
F 3 "" H 1250 5950 60  0001 C CNN
	1    1250 5950
	-1   0    0    1   
$EndComp
$Comp
L +3.3VP #PWR086
U 1 1 59A83711
P 1250 5725
F 0 "#PWR086" H 1300 5755 20  0001 C CNN
F 1 "+3.3VP" H 1250 5815 30  0000 C CNN
F 2 "" H 1250 5725 60  0001 C CNN
F 3 "" H 1250 5725 60  0001 C CNN
	1    1250 5725
	1    0    0    -1  
$EndComp
Text HLabel 1150 6175 0    60   Input ~ 0
TC1
$Comp
L CONN_3X1F TC1
U 1 1 59FC0530
P 1525 6175
F 0 "TC1" H 1628 6175 40  0000 L CNN
F 1 "CONN_3X1F" H 1628 6137 40  0001 L CNN
F 2 "modules:ED2601" H 1525 6175 60  0001 C CNN
F 3 "" H 1525 6175 60  0001 C CNN
	1    1525 6175
	1    0    0    -1  
$EndComp
Wire Wire Line
	1125 1625 1350 1625
Wire Wire Line
	1950 1525 2075 1525
Wire Wire Line
	2375 1525 2700 1525
Wire Wire Line
	2575 1525 2575 1975
Wire Wire Line
	2575 1975 2825 1975
Connection ~ 2575 1525
Wire Wire Line
	3125 1975 3450 1975
Wire Wire Line
	3450 1975 3450 1425
Wire Wire Line
	3300 1425 3500 1425
Connection ~ 3450 1425
Wire Wire Line
	2575 975  2575 1325
Wire Wire Line
	2400 975  2675 975 
Connection ~ 2575 975 
Wire Wire Line
	2975 975  3050 975 
Wire Wire Line
	1975 975  2100 975 
Wire Wire Line
	2025 1525 2025 2275
Wire Wire Line
	2025 2275 1600 2275
Connection ~ 2025 1525
Wire Wire Line
	1575 2050 2025 2050
Connection ~ 2025 2050
Wire Wire Line
	1275 2050 1175 2050
Wire Wire Line
	1175 1625 1175 2275
Connection ~ 1175 1625
Wire Wire Line
	1175 2275 1300 2275
Connection ~ 1175 2050
Wire Wire Line
	825  1625 675  1625
Wire Wire Line
	675  1625 675  1775
Wire Wire Line
	5025 750  4950 750 
Wire Wire Line
	4950 750  4950 1000
Wire Wire Line
	4950 1000 5100 1000
Wire Wire Line
	5100 1000 5100 1075
Wire Wire Line
	5325 750  5400 750 
Wire Wire Line
	5400 750  5400 1000
Wire Wire Line
	5400 1000 5300 1000
Wire Wire Line
	5300 1000 5300 1075
Wire Wire Line
	5200 1625 5200 1675
Wire Wire Line
	4475 1425 4850 1425
Wire Wire Line
	4700 1500 4700 1425
Connection ~ 4700 1425
Wire Wire Line
	5550 1425 6175 1425
Wire Wire Line
	5775 1500 5775 1425
Connection ~ 5775 1425
Wire Wire Line
	5775 1800 5775 1850
Wire Wire Line
	1550 1150 1550 1225
Wire Wire Line
	1350 1425 1200 1425
Wire Wire Line
	4700 1800 4700 1825
Wire Wire Line
	725  3950 725  4100
Wire Wire Line
	725  4000 1025 4000
Wire Wire Line
	1025 3950 1025 4200
Wire Wire Line
	1025 4200 1200 4200
Connection ~ 1025 4000
Wire Wire Line
	1500 4200 1575 4200
Wire Wire Line
	1575 4000 1575 5075
Wire Wire Line
	1575 4000 1725 4000
Wire Wire Line
	725  3650 725  3600
Wire Wire Line
	725  3600 1575 3600
Wire Wire Line
	1025 3600 1025 3650
Wire Wire Line
	1575 3200 1575 3800
Wire Wire Line
	1575 3800 1725 3800
Connection ~ 1025 3600
Wire Wire Line
	1575 3200 1700 3200
Connection ~ 1575 3600
Wire Wire Line
	2000 3200 2050 3200
Wire Wire Line
	2325 3900 2600 3900
Wire Wire Line
	2550 3200 2550 4750
Wire Wire Line
	2550 3200 2350 3200
Connection ~ 2550 3900
Wire Wire Line
	2900 3900 3350 3900
Wire Wire Line
	3025 3700 3025 3900
Connection ~ 3025 3900
Wire Wire Line
	3025 3250 3025 3400
Connection ~ 725  4000
Wire Wire Line
	3350 4100 3250 4100
Wire Wire Line
	3250 4100 3250 4500
Wire Wire Line
	3150 4500 3625 4500
Wire Wire Line
	4350 4500 3925 4500
Wire Wire Line
	4350 3475 4350 4500
Wire Wire Line
	4350 4000 3950 4000
Wire Wire Line
	4350 3475 4850 3475
Connection ~ 4350 4000
Wire Wire Line
	4850 3675 4725 3675
Wire Wire Line
	4725 3675 4725 4425
Wire Wire Line
	4725 3950 5550 3950
Wire Wire Line
	5550 3950 5550 3575
Wire Wire Line
	5550 3575 5450 3575
Wire Wire Line
	4725 4425 4825 4425
Connection ~ 4725 3950
Wire Wire Line
	1575 4475 1750 4475
Connection ~ 1575 4200
Wire Wire Line
	2550 4475 2050 4475
Wire Wire Line
	2550 4750 2400 4750
Connection ~ 2550 4475
Wire Wire Line
	5425 4525 5550 4525
Wire Wire Line
	5850 4525 6675 4525
Wire Wire Line
	5950 4525 5950 4975
Wire Wire Line
	5950 4975 4725 4975
Wire Wire Line
	6200 4875 6200 5025
Wire Wire Line
	6200 4925 6500 4925
Wire Wire Line
	6200 4525 6200 4575
Wire Wire Line
	6500 4525 6500 4575
Connection ~ 6500 4525
Connection ~ 6200 4925
Wire Wire Line
	6500 4925 6500 4875
Wire Wire Line
	4725 4975 4725 4625
Wire Wire Line
	4725 4625 4825 4625
Connection ~ 6200 4525
Connection ~ 5950 4525
Wire Wire Line
	1975 4750 2100 4750
Wire Wire Line
	2050 4750 2050 5075
Wire Wire Line
	2050 5075 1975 5075
Connection ~ 2050 4750
Wire Wire Line
	1575 4750 1675 4750
Connection ~ 1575 4475
Wire Wire Line
	1575 5075 1675 5075
Connection ~ 1575 4750
Wire Wire Line
	7750 3425 7675 3425
Wire Wire Line
	7675 3425 7675 3675
Wire Wire Line
	7675 3675 7825 3675
Wire Wire Line
	7825 3675 7825 3750
Wire Wire Line
	8050 3425 8125 3425
Wire Wire Line
	8125 3425 8125 3675
Wire Wire Line
	8125 3675 8025 3675
Wire Wire Line
	8025 3675 8025 3750
Wire Wire Line
	7925 4300 7925 4350
Wire Wire Line
	7200 4100 7575 4100
Wire Wire Line
	7425 4175 7425 4100
Connection ~ 7425 4100
Wire Wire Line
	8275 4100 8875 4100
Wire Wire Line
	8500 4175 8500 4100
Connection ~ 8500 4100
Wire Wire Line
	8500 4475 8500 4525
Wire Wire Line
	7425 4475 7425 4500
Wire Wire Line
	1550 1825 1550 1900
Wire Wire Line
	1550 1900 1800 1900
Wire Wire Line
	1925 4200 1925 4250
Wire Wire Line
	1925 4250 2150 4250
Connection ~ 3250 4500
Wire Wire Line
	3800 1425 3900 1425
Wire Wire Line
	1375 6275 1250 6275
Wire Wire Line
	1250 6275 1250 6375
Wire Wire Line
	1250 6100 1250 6175
Wire Wire Line
	1150 6175 1375 6175
Wire Wire Line
	1250 5725 1250 5800
Connection ~ 1250 6175
Wire Wire Line
	2575 1325 2700 1325
$Comp
L +3.3VP #PWR087
U 1 1 59FC0C38
P 1325 5800
F 0 "#PWR087" H 1375 5830 20  0001 C CNN
F 1 "+3.3VP" H 1325 5890 30  0000 C CNN
F 2 "" H 1325 5800 60  0001 C CNN
F 3 "" H 1325 5800 60  0001 C CNN
	1    1325 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1375 6075 1325 6075
Wire Wire Line
	1325 6075 1325 5800
$EndSCHEMATC
